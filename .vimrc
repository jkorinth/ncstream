filetype plugin on
autocmd FileType cpp setlocal ts=2 sw=2 et ai
autocmd FileType h setlocal ts=2 sw=2 et ai
autocmd FileType cmake setlocal ts=2 sw=2 et ai
function! RemoveTrailingWhitespace()
	try | %s/\s\s*$//g | catch | | endtry
endfunction
autocmd BufWritePre *.cpp call RemoveTrailingWhitespace()
autocmd BufWritePre *.c call RemoveTrailingWhitespace()
autocmd BufWritePre *.h call RemoveTrailingWhitespace()
autocmd BufWritePre CMakeLists.txt call RemoveTrailingWhitespace()
