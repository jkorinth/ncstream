cmake_minimum_required(VERSION 3.13)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
if (CLANG_TIDY_EXE)
  set(CMAKE_CXX_CLANG_TIDY clang-tidy -checks=-*,readability-*,cppcoreguidelines-*,modernize-*,hicpp-*,performance-*)
endif ()

add_subdirectory(src)
add_subdirectory(demo)
add_subdirectory(doc)
