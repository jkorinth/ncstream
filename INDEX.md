ncstream - ncursesw stream interface {#mainpage}
====================================

ncstream - ncursesw stream interface {#mainpage}
====================================

[TOC]

The `ncstream` library provides the `ncostream` class which is an `iostream`-
like implementation for the [ncurses][1] library. It supports

  *  RGB colors and gradient palette generation
  *  unicode output
  *  arbitrary cursor movements (full terminal screen)

It is geared toward two things: Simplifying the development of terminal based
apps in C++ (hiding the `ncurses` C API) by providing an extended output stream,
and enabling "sophisticated" graphics effects on the terminal in the vein of
C64 demos. It is _not_ a windowing/widget library for `ncurses` - if you need
layered windows, input forms and the like there are far better projects for
that, starting with the original `ncurses` C++ wrappers.

Building the library {#building}
--------------------

In the project root directory, run:

~~~
$ mkdir build && cd build && make -DCMAKE_BUILD_TYPE=Release .. && make
~~~

This will build the library and the demo. You can run `make install` to install
it system-wide, or just integrate this project into yours using, e.g., using
[`git subtree`][2].

Running the demo {#demo}
----------------

The demo `ncstream-demo` illustrates the library capabilities and can be run
without arguments:

~~~
$ cd build && ./ncstream-demo
~~~

Minimal Example {#minimal-example}
---------------

~~~{.cpp}
#include <ncs/ncostream.h>
#include <chrono>
#include <thread>

int main(int argc, char *argv[]) {
  ncs::ncostream nc {} << ncs::bold << "Hello world!" << ncs::refresh;
  std::this_thread::sleep_for(std::chrono::seconds(3));
}
~~~

Clearing the screen {#clearing}
-------------------

The screen or the rest of the current line can be cleared:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::clear << "Screen is empty";
nc << ncs::clrtoeol << "rest of current line is empty";
nc << ncs::refresh << "must be issued to update the screen";
~~~

Text Output {#text-output}
-----------

Works similar to `ostream`:

~~~{.cpp}
nc << "Regular text, advances the cursor."
   << L"Unicode text, advances the cursor."
   << std::string("works as well")
   << std::wstring(L"unicode too");
~~~

Cursor Movement {#cursor}
---------------

Screen size can be determined using `screen_extent()` on a `ncs::ncostream`:
~~~{.cpp}
ncs::ncostream nc {};
nc << "screen: " << nc.screen_extent().width_ << " x "
                 << nc.screen_extent().height_ << " characters ";
~~~

Cursor can be moved to an absolute position:

~~~{.Cpp}
ncs::ncostream nc {};
nc << "some output at current cursor pos"
   << ncs::pos { ncs::row { 10 }, ncs::col { 10 } }
   << "this will be output at (10, 10)";
~~~

Cursor can be moved relative to its current position:

~~~{.cpp}
ncs::ncostream nc {};
nc << "first line"
   << ncs::v { 1 }
   << "one line down"
   << ncs::h { -10 }
   << "10 chars back"
   << ncs::rel_pos { ncs::v { 5 }, ncs::h { 10 } }
   << "moved 5 rows down, 10 columns to the right";
~~~

You can change the visibility of the cursor itself:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::show_cursor << "normal cursor at cursor position"
   << ncs::hide_cursor << "cursor hidden"
   << ncs::emph_cursor << "cursor very visible (if supported)";
~~~

Attributes {#attributes}
----------

Depending on the terminal (emulator) capabilities, `ncurses` support several
font attributes:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::normal << "regular font" << ncs::reset
   << ncs::underline << "underlined font" << ncs::reset
   << ncs::reverse << "reversed colors" << ncs::reset
   << ncs::blink << "blinking font" << ncs::reset
   << ncs::dim << "dimmed font" << ncs::reset
   << ncs::bold << "bold font" << ncs::reset
   << ncs::italic << "italic font" << ncs::reset
   << ncs::invis << "invisible font" << ncs::reset
   << ncs::italic << ncs::bold << "combinations are possible";
~~~

Attributes can be or'ed in the regular way:

~~~{.cpp}
auto my_font_attr { ncs::italic | ncs::bold };
~~~

Colors {#colors}
------

Check terminal color capabilities:

~~~{.cpp}
ncs::ncostream nc {};
if (nc.has_color())
  ...
auto max_number_of_colors_supported { nc.max_number_of_colors() };
auto max_number_of_color_pairs { nc.max_number_of_color_pairs() };
~~~

Colors are defined by `ncs::color::color`:

~~~{.cpp}
auto white {
  ncs::color::color {
    ncs::color::r { 1.0 },
	ncs::color::g { 1.0 },
	ncs::color::b { 1.0 } } };
~~~

Before use, colors must be registered with `ncurses`:

~~~{.cpp}
ncs::color::color_id id { ncs::color::get_color(white) };
~~~

To apply the color, a `pair` must be generated consisting of foreground (i.e.,
font) and background colors:

~~~{.cpp}
ncs::color_pair_id id { ncs::color::get_pair({
  ncs::color::red,		// --> foreground
  ncs::color::blue		// --> background
}) };
~~~

This can then be used to change the current color:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::manipulator::set_color_pair { id }
   << "this text will be red on blue";
~~~

Colors can also be set at arbitrary positions:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::manipulator::change_color {
        ncs::pos { ncs::row {10}, ncs::col {10} },
		id
      }
   << "character at (10,10) will now be red on blue";
~~~

The color of the cursor can be changed as well:

~~~{.cpp}
ncs::ncostream nc {};
nc << ncs::manipulator::set_cursor_color { ncs::black }
   << "cursor should now be blue (not supported on all terminals)";
~~~

Color gradients can be generated using helpers:

~~~{.cpp}
auto grad { ncs::color::Gradient<>(ncs::color::yellow) };		// start color
// arbitrary number of steps in relative distance can be added:
grad << Gradient<>::Step { ncs::color::white, 0.1 }
     << Gradient<>::Step { ncs::color::red, 0.1 };
// grad now: 1/2 yellow -> white, 1/2 white to red;
grad << Gradient<>::Step { ncs::color::orange, 0.4 };
// grad now: 1/4 yellow -> white, 1/4 white to red, 1/2 red -> orange
// to generate the gradient with 16 steps:
std::vector<ncs::color::color> grad_colors {
  grad.gradient(16)
};
// same gradient with 128 steps:
std::vector<ncs::color::color> grad_colors_large {
  grad.gradient(128)
};
~~~

For low-level control, `ncurses` colors can be manipulated directly:

~~~{.cpp}
ncs::color::set_color(0, ncs::color::black);	// set color 0 to black
~~~

Notes {#notes}
-----

  *  most capabilities are dependent on the terminal emulator you're using and its
     configuration
  *  e.g., if colors don't work, try `export TERM=xterm-256color` in the terminal,
     then run the test again.
  *  `ncurses` is stateful, so setting any attribute or color will remain until reset
  *  `ncostream` is not thread-safe
  *  `ncostream` should only be instantiated once per application (`ncurses` is not
     shareable)
  *  destruction of the `ncostream` will end `ncurses` mode and reset the screen
  *  `std::cout` and `std::cin` will not work as expected while `ncostream` exists

Further information {#further-info}
-------------------

Check the demo code in `demo` to understand further details of how to use the
library.

**Have fun!**

[1]: https://www.gnu.org/software/ncurses/
[2]: https://www.atlassian.com/blog/git/alternatives-to-git-submodule-git-subtree
