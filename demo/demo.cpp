#include <cassert>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include "doom_fire.h"
#include "matrix.h"
#include "ncs/color/gradient.h"
#include "ncs/ncostream.h"
#include "throttler.h"
#include "trinity.h"

using namespace std::literals::chrono_literals;

static Throttler update_throttler {  5000us };
static Throttler render_throttler { 20000us };

static std::string make_stats(const Matrix& m) {
  static auto start { std::chrono::high_resolution_clock::now() };
  auto d { std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::high_resolution_clock::now() - start) };
  std::stringstream ss {};
  ss << std::fixed << std::setprecision(2) << std::setw(8)
     << " FPS: " << m.render_cnt() * 1000.0 / (d.count() + 1)
     << " UPD: " << m.update_cnt() * 1000.0 / (d.count() + 1)
     << " ";
  return ss.str();
}

template <typename U>
static void update(U&& u, volatile bool& stop) {
  while (!stop) {
    u.update();
    update_throttler.throttle();
  }
}

template <typename R>
static void render(const R& r, volatile bool& stop) {
  while (!stop) {
    r.render();
    render_throttler.throttle();
  }
}

void render(Matrix& m, ncs::ncostream& nc, volatile bool& stop) {
  static auto colors { ncs::color::get_pair({
                       ncs::color::green,
                       ncs::color::black }) };
  while (!stop) {
    m.render();
    auto s { make_stats(m) };
    const auto se { nc.screen_extent() };
    nc << ncs::pos{ ncs::row { se.height_ - 2 },
                    ncs::col { (se.width_ - static_cast<ncs::col::type>(s.size())) / 2} }
       << ncs::manipulator::set_color_pair { colors }
       << s
       << ncs::refresh;
    render_throttler.throttle();
  }
}

static void print_stats() {
  const auto& ut = update_throttler;
  const auto& rt = render_throttler;

  std::cout << "Update Throttler: "
            << ut.sleep_ << "us "
            << "idle: "
            << std::fixed << std::setprecision(2)
            << ut.sleep_ / ut.period_.count() * 100.0 << "% "
            << std::endl;
  std::cout << "Render Throttler: "
            << rt.sleep_ << "us "
            << "idle: "
            << std::fixed << std::setprecision(2)
            << rt.sleep_ / rt.period_.count() * 100.0 << "% "
            << std::endl;
}

int main(int argc, char* argv[]) {
  {
    using namespace std::literals::chrono_literals;
    ncs::ncostream nc {};
    volatile bool stop { false };

		// make black true black
    ncs::color::set_color(0, ncs::color::black);

    DoomFire df { nc };
    std::thread r { [&df, &stop] () { render(df, stop); } };
    std::thread u { [&df, &stop] () { update(df, stop); } };
    std::this_thread::sleep_for(10s);
    stop = true;
    u.join();
    r.join();

    Trinity t { nc };
    stop = false;
    auto now { std::chrono::high_resolution_clock::now() };
    r = std::thread { [&t, &stop] () { render(t, stop); } };
    u = std::thread { [&t, &stop] () {
      while (!(stop = t.update()))
        update_throttler.throttle();
    } };
    u.join();
    r.join();

    Matrix m { nc };
    stop = false;
    u = std::thread { [&m, &stop] () { update(m, stop); } };
    r = std::thread { [&m, &nc, &stop] () { render(m, nc, stop); } };
    while (!stop) {
      std::this_thread::sleep_for(10s);
      stop = std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::high_resolution_clock::now() - now).count() > 20;
    }
    u.join();
    r.join();
  }
  print_stats();
}
