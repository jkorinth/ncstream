#include "doom_fire.h"
#include <iterator>
#include <iostream>
#include <random>
#include "stopwatch.h"

namespace {
std::mt19937 rnd {};
}   // namespace

DoomFire::DoomFire(ncs::ncostream& nc) noexcept
  : nc_(nc),
    sc_(nc.screen_extent()) {
  const auto fire { make_fire_gradient(nc.screen_extent().height_ * 2.5) };
  for (const auto& c : fire) {
    fire_.push_back(ncs::color::get_pair({c, c}));
  }

  for (int r = 0; r < sc_.height_; ++r) {
    std::vector<int> line {};
    for (int c = 0; c < sc_.width_; ++c) {
      line.push_back(fire.size() - 1);
      nc_ << " ";
    }
    map_.push_back(line);
  }
  new_map_ = std::vector<std::vector<int>>(map_);
  must_render_ = std::vector<std::vector<int>>(map_);
  ncs::color::set_color(0, ncs::color::black);
  nc_ << ncs::clear << ncs::hide_cursor;
}

DoomFire::~DoomFire() noexcept = default;

void DoomFire::update() {
  std::lock_guard<std::mutex> l { update_mtx_ };
  static Stopwatch total {};
  static Stopwatch sw {};
  if (sw.count<std::chrono::milliseconds>().count() > 1000 / sc_.height_) {
    if (total.count<std::chrono::seconds>().count() < 6 && rnd() % 10 < 8)
      stoke();
    else
      quench();

    for (int c = 0; c < sc_.width_; ++c) {
      for (int r = 0; r < sc_.height_ - 1; ++r) {
        const int lr { static_cast<int>(rnd() % 2) - 1 };
        const auto decay { rnd() % 8 + 1 };
        new_map_[r][c] = std::clamp<int>(
            map_[r + 1][std::clamp<int>(c + lr, 0, sc_.width_)] + decay,
            0,
            fire_.size() - 1);
        must_render_[r][c] = must_render_[r][c] || new_map_[r][c] != map_[r][c];
      }
    }
    std::swap(new_map_, map_);
    sw.restart();
  }
}

void DoomFire::render() const {
  std::lock_guard<std::mutex> l { update_mtx_ };
  for (int r = 0; r < sc_.height_ - 1; ++r) {
    for (int c = 0; c < sc_.width_; ++c) {
      if (must_render_[r][c]) {
        ncs::pos p { ncs::row { r }, ncs::col { c } };
        nc_ << ncs::manipulator::change_color { p, fire_[map_[r][c]] };
        must_render_[r][c] = false;
      }
    }
  }
  nc_ << ncs::refresh;
}

void DoomFire::stoke() {
  for (int c = 0; c < sc_.width_; ++c) {
    auto h { rnd() % 4 + 1 };
    for (auto d { h }; d > 0; --d) {
      map_[sc_.height_ - d][c] = 0;
      must_render_[sc_.height_ - d][c] = true;
    }
  }
}

void DoomFire::quench() {
  for (int c = 0; c < sc_.width_; ++c) {
    auto h { rnd() % 4 + 1 };
    for (auto d { h }; d > 0; --d) {
      map_[sc_.height_ - d][c] = std::clamp<int>(
          map_[sc_.height_ - d][c] + fire_.size() / 10, 0, fire_.size() - 1
      );
      must_render_[sc_.height_ - d][c] = true;
    }
  }
}
