#ifndef NCSTREAM_DEMO_DOOM_FIRE_H_
#define NCSTREAM_DEMO_DOOM_FIRE_H_

#include <algorithm>
#include <array>
#include <mutex>
#include <vector>
#include "ncs/color/color.h"
#include "ncs/color/gradient.h"
#include "ncs/ncostream.h"

template<long N>
inline std::array<ncs::color::color, N> make_fire_gradient() {
  using ncs::color::gradient;
  return (gradient<>(ncs::color::yellow)
      << gradient<>::step { ncs::color::white, 0.0 }
      << gradient<>::step { ncs::color::yellow, 0.1 }
      << gradient<>::step { ncs::color::orange, 0.2 }
      << gradient<>::step { ncs::color::red, 0.5 }
      << gradient<>::step { ncs::color::black, 1.0 }).make<N>();
}

inline std::vector<ncs::color::color> make_fire_gradient(long N) {
  using ncs::color::gradient;
  return (gradient(ncs::color::yellow)
      << gradient<>::step { ncs::color::white, 0.0 }
      << gradient<>::step { ncs::color::yellow, 1.0 }
      << gradient<>::step { ncs::color::orange, 1.0 }
      << gradient<>::step { ncs::color::red, 1.0 }
      << gradient<>::step { ncs::color::black, 1.0 }).make(N);
}

class DoomFire {
 public:
  explicit DoomFire(ncs::ncostream& nc) noexcept;
  DoomFire(const DoomFire& rhs) = delete;
  DoomFire(DoomFire&& rrs) = delete;
  DoomFire& operator =(const DoomFire& rhs) = delete;
  DoomFire& operator =(DoomFire&& rrs) = delete;
  virtual ~DoomFire() noexcept;

  void update();
  void render() const;

 private:
  void stoke();
  void quench();
  ncs::ncostream& nc_;
  const ncs::extent sc_;
  std::vector<ncs::color::color_pair_id> fire_ {};
  std::vector<std::vector<int>> map_ {};
  std::vector<std::vector<int>> new_map_ {};
  mutable std::vector<std::vector<int>> must_render_ {};
  mutable std::mutex update_mtx_ {};
};

#endif  // NCSTREAM_DEMO_DOOM_FIRE_H_
