#include "matrix.h"
#include <chrono>
#include <iomanip>
#include <sstream>
#include <random>
#include "ncs/ncostream.h"
#include "stopwatch.h"

using namespace ncs;

namespace {
using namespace ncs;

static std::mt19937_64 rnd {};

static constexpr color::color bg_ { color::black };
static constexpr color::color fg_ { color::r {450}, color::g {1000}, color::b {250}
};

static constexpr std::array<color::color, Matrix::TRAIL_LEN> make_colors() {
  std::array<color::color, Matrix::TRAIL_LEN> shades {
    color::interpolate<Matrix::TRAIL_LEN>(bg_, fg_)
  };
  shades[Matrix::TRAIL_LEN - 1] = color::white;
  return shades;
}

static const std::array<color::color_pair_id, Matrix::TRAIL_LEN> make_shades() {
  const auto shades { make_colors() };
  std::array<color::color_pair_id, Matrix::TRAIL_LEN> ret {};
  for (int i = 0; i < Matrix::TRAIL_LEN; ++i) {
    ret[i] = color::get_pair({ shades[i], color::black });
  }
  return ret;
}

}  // namespace

struct Matrix::Trail {
  pos pos_;
  long length_;
  double speed_;
  double row_;
};

Matrix::Matrix(ncostream& nc) : nc_(nc) {
  const auto se { nc_.screen_extent() };
  static const auto fgbg { color::get_pair({bg_, color::black}) };
  nc_ << hide_cursor;
  nc_ << ncs::clear << manipulator::set_color_pair { fgbg };
  for (long i = se.width_ * se.height_; i > 0; --i)
    nc_ << gen_glyph_();
  nc_ << ncs::refresh;
  for (long i = se.width_; i > 0; --i) {
    trails_.push_back({
        pos { row {0}, col {static_cast<col::type>(rnd() % se.width_)} },
        TRAIL_LEN,
        (rnd() % 14000 + 2000) / 1000.0f,  // 2-16 rows per second
        0.0f,
    });
  }
}

Matrix::~Matrix() noexcept = default;

void Matrix::update() {
  using namespace std::chrono;
  using elapsed = microseconds;

  static Stopwatch sw {};
  auto d { sw.count<elapsed>() };

  // update the color trails
  const auto se { nc_.screen_extent() };
  if (d.count()) {
    for (auto& t : trails_) {
      t.row_ += t.speed_ * d.count() / elapsed::period::den;
      t.pos_.row_ = ncs::row {std::lroundf(t.row_)};
      if (t.pos_.row_ >= se.height_ + t.length_) {
        t.pos_   = pos { row {0}, col {static_cast<col::type>(rnd() % se.width_)} };
        t.row_   = 0.0;
        t.speed_ = gen_speed();
      }
    }
  }
  sw.restart();
  ++update_cnt_;
}

void Matrix::render() const {
  using namespace std::chrono;
  using elapsed = microseconds;

  static Stopwatch sw;
  static auto d { sw.count<elapsed>() };

  static const auto fgbg { color::get_pair({color::black, color::black}) };
  static double glyphs_to_change { 0.0 };

  // change 10% of visible glyphs per second
  const auto se { nc_.screen_extent() };
  const double max_num_glyphs { static_cast<double>(se.width_ * se.height_ / 5) };
  glyphs_to_change += max_num_glyphs * d.count() / elapsed::period::den;
  const long lim { static_cast<long>(std::floor(glyphs_to_change)) };

  if (lim > 0) {
    for (long i = lim; i >= 0; --i, --glyphs_to_change) {
      row r { static_cast<row::type>(rnd() % se.height_) };
      col c { static_cast<col::type>(rnd() % se.width_)  };
      nc_ << pos { r, c } << manipulator::set_color_pair { fgbg } << gen_glyph_();
    }
  }

  // change color in the trails
  static const auto shades_pairs { make_shades() };
  for (const auto& t : trails_) {
    pos p { t.pos_ };
    for (int i = TRAIL_LEN - 1; i > 0 && p.row_ >= 0; --i, p = p - v{1}) {
      auto const cp { shades_pairs[i] };
      if (p.row_ < se.height_) {
        nc_ << manipulator::change_color { p, cp };
      }
    }
  }
  nc_ << ncs::refresh;
  sw.restart();
  ++render_cnt_;
}

wchar_t Matrix::gen_ethiopic() { return L'\u1200' + rnd() % 0x47; }

double Matrix::gen_speed() {
  return (rnd() % 12000 + 2000) / 1000.0f;  // 2-14 rows per second
}
