#ifndef NCSTREAM_DEMO_MATRIX_H_
#define NCSTREAM_DEMO_MATRIX_H_

#include <array>
#include <functional>
#include <vector>
#include "ncs/ncostream.h"

class Matrix {
 public:
  static constexpr long TRAIL_LEN { 16 };

  explicit Matrix(ncs::ncostream& nc);
  Matrix(const Matrix& rhs) = delete;
  Matrix(Matrix&& rrs) = delete;
  Matrix& operator =(const Matrix& rhs) = delete;
  Matrix& operator =(Matrix&& rrs) = delete;
  virtual ~Matrix() noexcept;

  void update();
  void render() const;

  inline uint64_t render_cnt() const noexcept { return render_cnt_; }
  inline uint64_t update_cnt() const noexcept { return update_cnt_; }

 private:
  struct Trail;

  ncs::ncostream& nc_;
  std::function<wchar_t()> gen_glyph_ { gen_ethiopic };
  std::vector< Trail > trails_;

  mutable uint64_t render_cnt_ {};
  mutable uint64_t update_cnt_ {};

  static wchar_t gen_ethiopic();
  static double gen_speed();
};

#endif  // NCSTREAM_DEMO_MATRIX_H_
