#ifndef NCSTREAM_DEMO_STOPWATCH_H_
#define NCSTREAM_DEMO_STOPWATCH_H_

#include <chrono>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::time_point;

class Stopwatch {
 public:
  template <class Rep, class Period>
  inline duration<Rep, Period> count() const noexcept {
    return duration_cast<Rep, Period>(high_resolution_clock::now() - last_);
  }

  template <class T>
  inline T count() const noexcept {
    return duration_cast<T>(high_resolution_clock::now() - last_);
  }

  inline void restart() noexcept {
    last_ = high_resolution_clock::now();
  }

 private:
  time_point<high_resolution_clock> last_ { high_resolution_clock::now() };
};

#endif  // NCSTREAM_DEMO_STOPWATCH_H_
