#ifndef NCSTREAM_THROTTLER_H_
#define NCSTREAM_THROTTLER_H_

#include <chrono>
#include <thread>
#include "walking_average.h"

template <typename U>
class Throttler {
 public:
  Throttler(U period) : period_(period) {}
  virtual ~Throttler() noexcept = default;

  void throttle() {
    const auto now { std::chrono::high_resolution_clock::now() };
    const auto d { std::chrono::duration_cast<U>(now - last_) };
    last_ = std::chrono::high_resolution_clock::now();
    if (d < period_) {
      sleep_ += (period_ - d).count();
      std::this_thread::sleep_for(period_ - d);
    }
  }

 private:
  std::chrono::time_point<std::chrono::high_resolution_clock> last_
      { std::chrono::high_resolution_clock::now() };
 public:
  const U period_;
  WalkingAverage<float> sleep_ {};
};


#endif  // NCSTREAM_THROTTLER_H_
