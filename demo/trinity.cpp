#include "trinity.h"
#include <algorithm>
#include <array>
#include <deque>
#include <iostream>
#include <iterator>
#include <map>
#include <mutex>
#include <numeric>
#include <string>
#include <vector>
#include "ncs/ncostream.h"
#include "stopwatch.h"

using namespace std::chrono;

static constexpr int TIME_FOR_ONE_LINE { 1500 };

struct Trinity::Implementation {
  std::mutex impl_mtx_ {};
  const std::array<std::string, 4> lines_ {
    "Wake up, Neo...",
    "The matrix has you...",
    "Follow the white rabbit.",
    "Knock, knock, Neo."
  };
  int curr_line { 0 };
  std::string::const_iterator cit { std::begin(lines_[curr_line]) };
};

static constexpr char to_lower(char character) {
  if (character >= 'A' && character <= 'Z')
    return character + ('a' - 'A');
  return character;
}

constexpr int finger_for_key(char character) {
  const auto c { to_lower(character) };
  if (c == 't'  || c == 'y'  || c == 'u'  || c == 'f'  || c == 'g'  ||
      c == 'h'  || c == 'j'  || c == 'c'  || c == 'v'  || c == 'b'  ||
      c == 'n'  || c == 'm') {
    return 1;
  } else
  if (c == 'e'  || c == 'r'  || c == 'i'  || c == 'o'  || c == 'd'  ||
      c == 'k'  || c == 'x' || c == ',') {
    return 2;
  } else
  if (c == 'q'  || c == 'w'  || c == 'o'  || c == 'p'  || c == 's'  ||
      c == 'l'  || c == 'z'  || c == '.') {
    return 3;
  } else
  if (c == 'a' || c == ';') {
    return 4;
  }
  return 5;
}

static const std::deque<double> make_delays(const std::string& s) {
  char p { '\0' };
  double v {};
  std::deque<double> delays {};
  for (const auto c : s) {
    if (p) {
      v = (finger_for_key(c) != finger_for_key(p) ? 1 : 2) * finger_for_key(c);
      delays.push_back(v);
    }
    p = c;
  }
  const auto sum { std::accumulate(std::begin(delays), std::end(delays), 0) };
  std::transform(std::begin(delays), std::end(delays), std::begin(delays),
      [sum] (auto x) { return (x / sum) * TIME_FOR_ONE_LINE; });
  return delays;
}

Trinity::Trinity(ncs::ncostream& nc)
  : nc_(nc), impl_(std::make_unique<Implementation>()) {}

Trinity::~Trinity() noexcept = default;

bool Trinity::update() {
  static Stopwatch sw {};
  std::scoped_lock sl { impl_->impl_mtx_ };
  auto d { sw.count<std::chrono::milliseconds>().count() };

  unsigned cl { static_cast<unsigned>(d / (TIME_FOR_ONE_LINE * 2)) };
  impl_->curr_line = cl < impl_->lines_.size() ? cl : impl_->curr_line;

  auto delays { make_delays(impl_->lines_[impl_->curr_line]) };
  impl_->cit = std::next(std::begin(impl_->lines_[impl_->curr_line]));
  d %= TIME_FOR_ONE_LINE * 2;
  while (delays.size() && delays.front() <= d) {
    d -= delays.front();
    delays.pop_front();
    if (impl_->cit != std::end(impl_->lines_[impl_->curr_line]))
      impl_->cit++;
  }
  return static_cast<unsigned>(sw.count<std::chrono::milliseconds>().count()) >
    2 * TIME_FOR_ONE_LINE * impl_->lines_.size();
}

void Trinity::render() const {
  std::scoped_lock sl { impl_->impl_mtx_ };
  static auto colors { ncs::color::get_pair({ ncs::color::green,
      ncs::color::black }) };
  nc_ << ncs::manipulator::set_color_pair {colors}
      << ncs::clrtoeol
      << ncs::pos { ncs::row {2}, ncs::col {4} }
      << ncs::manipulator::set_cursor_color { ncs::color::green }
      << std::string { std::begin(impl_->lines_[impl_->curr_line]), impl_->cit }
      << ncs::refresh;
}
