#ifndef NCSTREAM_DEMO_TRINITY_H_
#define NCSTREAM_DEMO_TRINITY_H_

#include <array>
#include <chrono>
#include <memory>
#include <string>
#include "ncs/ncostream.h"

class Trinity {
 public:
  explicit Trinity(ncs::ncostream& nc);
  Trinity(const Trinity& rhs) = delete;
  Trinity(Trinity&& rrs) = delete;
  Trinity& operator =(const Trinity& rhs) = delete;
  Trinity& operator =(Trinity&& rrs) = delete;
  virtual ~Trinity() noexcept;

  bool update();
  void render() const;

 private:
  ncs::ncostream& nc_;
  std::chrono::time_point<std::chrono::high_resolution_clock> start_ {
    std::chrono::high_resolution_clock::now()
  };
  struct Implementation;
  std::unique_ptr<Implementation> impl_;
};

#endif  // NCSTREAM_DEMO_TRINITY_H_
