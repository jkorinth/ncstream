#ifndef NCSTREAM_WALKING_AVERAGE_H_
#define NCSTREAM_WALKING_AVERAGE_H_

#include <type_traits>

template <typename T,
          typename = std::enable_if_t<std::is_floating_point_v<T>, void>>
class WalkingAverage {
 public:
   template <typename U>
   WalkingAverage& operator +=(U f) noexcept {
     value_ += (f - value_) / ++count_;
     return *this;
   }

   inline operator T() const noexcept { return value_; }

   void reset() noexcept {
     value_ = 0;
     count_ = 0;
   }

   inline uint64_t count() const noexcept { return count_; }

 private:
  T value_ {};
  uint64_t count_ {};
};

#endif  // NCSTREAM_WALKING_AVERAGE_H_

