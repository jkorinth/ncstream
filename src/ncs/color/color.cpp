#include "ncs/color/color.h"
extern "C" {
#include <ncursesw/ncurses.h>
}
#include <optional>
#include <map>
#include <string>
#include "ncs/types.h"

namespace ncs {
namespace color {
namespace {

inline constexpr uint64_t to_hash(const color c) {
  return c.red_.operator r::underlying_type() * 1000 * 1000 +
         c.green_.operator r::underlying_type() * 1000 +
         c.blue_.operator r::underlying_type();
}

inline constexpr uint64_t to_hash(const pair& p) {
  return (to_hash(p.fg_) << 32) + to_hash(p.bg_);
}

static std::map<uint64_t, color_id> cmap {};

static std::map<uint64_t, color_pair_id> cpmap {};

}  // namespace

color_id get_color(const color c) {
  static color_id next_id { 16 };
  auto it = cmap.find(to_hash(c));
  if (it == cmap.end()) {
    auto id { next_id++ };
    cmap.insert({ to_hash(c), id });
    init_color(id, c.red_, c.green_, c.blue_);
    return id;
  }
  return it->second;
}

color_pair_id get_pair(const pair& p) {
  static color_pair_id next_id { 16 };
  auto it = cpmap.find(to_hash(p));
  if (it == cpmap.end()) {
    auto id { next_id++ };
    cpmap.insert({ to_hash(p), id });
    init_pair(id, get_color(p.fg_), get_color(p.bg_));
    return id;
  }
  return it->second;
}

void set_color(color_id id, color c) {
  init_color(id, c.red_, c.green_, c.blue_);
}

}  // namespace color
}  // namesapce ncs
