#ifndef NCSTREAM_NCS_COLOR_COLOR_H_
#define NCSTREAM_NCS_COLOR_COLOR_H_

#include <array>
#include <cassert>
#include <cmath>
#include <functional>
#include <limits>
#include <optional>
#include <ostream>
#include <string>
#include <type_traits>
#include <vector>

namespace ncs {
/** \brief Namespace color contains supporting functions for color management
 *         and color support. */
namespace color {

/** \brief Represents a single component value in a color. **/
class component {
 public:
  using underlying_type = short;    /**< Underlying value representation. **/
  /** \brief Minimum value. **/
  static constexpr underlying_type min { 0 };
  /** \brief Maximum value. **/
  static constexpr underlying_type max { 1000 };
  /** \brief Clamp input to range [min, max]. **/
  static constexpr underlying_type clamp(underlying_type v) {
    return v > max ? max : v < min ? min : v;
  }

  /** \brief Construct component from value (clamped). **/
  explicit constexpr component(underlying_type value = min)
    : value_(clamp(value)) {}

  /** \brief Construct component from float (relative to range). **/
  template <typename T,
            typename std::enable_if_t<std::is_floating_point_v<T>, int> = 0>
  explicit constexpr component(T v)
    : value_(clamp(static_cast<int>(v * max))) {}

  /** \return Value. **/
  constexpr operator underlying_type() const noexcept { return value_; }

  /**
   * \brief Convert value to other integral type.
   *
   * Converts the component value to another type using `std::numeric_limits`
   * on `T` to scale the underlying value into [min, max] of `T`.
   *
   * \tparam T Type to convert to (full range will be used).
   **/
  template <typename T>
  constexpr T to() const noexcept {
    return static_cast<T>((value_ - min) / static_cast<double>(max - min) *
        std::numeric_limits<T>::max());
  }

 private:
  underlying_type value_;
};

using color_id      = short;    /**< \brief Color identifier. **/
using color_pair_id = short;    /**< \brief Color pair (fg/bg) identifier. **/

/** \brief Red component. **/
struct r : public component { using component::component; };
/** \brief Green component. **/
struct g : public component { using component::component; };
/** \brief Blue component. **/
struct b : public component { using component::component; };

/**
 * \brief RGB color value.
 **/
struct color {
  r red_;     /**< Red component. **/
  g green_;   /**< Green component. **/
  b blue_;    /**< Blue component. **/
};

/**
 * \brief RGB Color pair (foreground and background).
 * TODO add type tags for foreground and background
 **/
struct pair {
  color fg_;  /**< Foreground color. **/
  color bg_;  /**< Background color. **/
};

/** \return Id of color after registration. **/
color_id get_color(const color c);
/** \return Id of color pair after registration. **/
color_pair_id get_pair(const pair& c);

constexpr color white   { r{1.0}, g{1.0}, b{1.0} }; /**< \brief RGB white **/
constexpr color black   { r{0.0}, g{0.0}, b{0.0} }; /**< \brief RGB black **/
constexpr color red     { r{1.0}, g{0.0}, b{0.0} }; /**< \brief RGB red **/
constexpr color green   { r{0.0}, g{1.0}, b{0.0} }; /**< \brief RGB green **/
constexpr color blue    { r{0.0}, g{0.0}, b{1.0} }; /**< \brief RGB blue **/
constexpr color yellow  { r{1.0}, g{1.0}, b{0.0} }; /**< \brief RGB yellow **/
constexpr color orange  { r{1.0}, g{0.5}, b{0.0} }; /**< \brief RGB orange **/

/**
 * \brief Linear interpolation of two colors in N steps.
 *
 * Linear, component-wise interpolation between `c1` and `c2` in n steps;
 * callback function will be called n times with color values.
 * Function is `constexpr`, can be used at compile-time.
 **/
template <typename F>
constexpr inline void interpolate(long n, color c1, color c2, const F& cb) {
  static_assert(std::is_constructible_v<std::function<void(color)>, F>,
      "callback function must be of type void(color)");
  assert(n > 0);
  for (int i = 0; i < n; ++i) {
    auto f { i / static_cast<float>(n) };
    cb({
      r{static_cast<r::underlying_type>(std::lroundf((c1.red_ + (c2.red_ - c1.red_) * f)))},
      g{static_cast<g::underlying_type>(std::lroundf((c1.green_ + (c2.green_ - c1.green_) * f)))},
      b{static_cast<b::underlying_type>(std::lroundf((c1.blue_ + (c2.blue_ - c1.blue_) * f)))}
    });
  }
}

/**
 * \brief Linear interpolation of two colors in N steps.
 *
 * Linear, component-wise interpolation between `c1` and `c2` in N steps.
 * Function is `constexpr`, can be used at compile-time.
 * \return N element array of color.
 **/
template <long N>
constexpr std::array<color, N> interpolate(color c1, color c2) {
  std::array<color, N> ret {};
  interpolate(N, c1, c2, [&ret] (color c) {
      static int cn { 0 };
      ret[cn] = c;
      ++cn;
  });
  return ret;
}

/**
 * \brief Linear interpolation of two colors in N steps.
 *
 * Linear, component-wise interpolation between `c1` and `c2` in N steps.
 * \return N element array of color.
 **/
inline std::vector<color> interpolate(long N, color c1, color c2) {
  assert(N > 0);
  std::vector<color> ret {};
  interpolate(N, c1, c2, [&ret] (color c) { ret.push_back(c); });
  return ret;
}

/** \brief Changes the definition of color id. **/
void set_color(color_id id, color c);

/** \brief Stream output operator for color. **/
inline std::ostream& operator<<(std::ostream& o, const ncs::color::color& c) {
  o << "RGB(" << c.red_ << "," << c.green_ << "," << c.blue_ << ")";
  return o;
}

}  // namespace color
}  // namespace ncs

#endif  // NCSTREAM_NCS_COLOR_COLOR_H_
