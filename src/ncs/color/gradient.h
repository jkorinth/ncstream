#ifndef NCSTREAM_NCS_COLOR_GRADIENT_H_
#define NCSTREAM_NCS_COLOR_GRADIENT_H_

#include <iterator>
#include <utility>
#include <vector>
#include "ncs/color/color.h"

namespace ncs {
namespace color {

/**
 * \brief Support class to generate linear color gradients.
 *
 * Used as an intermediate to construct linear color gradients.
 * _Usage example_:
 * ~~~{.cpp}
 * using ncs::color::gradient::gradient;
 * gradient g {};
 * g << gradient::step { ncs::color::white, 0.0f }
 *   << gradient::step { ncs::color::red,   0.5f }
 *   << gradient::step { ncs::color::black, 1.0f };
 * auto gc { g.gradient(16) };  // generate 16 step gradient
 * ~~~
 *
 * \tparam V Type to represent step values.
 **/
template <typename V = float>
class gradient {
 public:
  /**
   * \brief A step denotes a color at a position within the gradient.
   *
   * Position is relative to overall sum of steps and will be scaled.
   * _Example_: 0.0, 0.5, 1.0 defines a three-color gradient where the second
   * color has equal distance to both other colors. A different way of writing
   * this would be 0, 1, 2.
   **/
  struct step {
    ncs::color::color col_;   /**< Color at the step. **/
    V at_;                    /**< Distance from previous color (unitless). **/
  };

  /** \brief Construct gradient starting at black. **/
  gradient() noexcept = default;
  /** \brief Construct gradient starting at `start_color`. **/
  explicit gradient(color start_color) noexcept : start_(start_color) {}

  /** \brief Add a step to the gradient. **/
  gradient& operator +=(step s) noexcept {
    if (steps_.size() == 0)
      steps_.push_back(step { s.col_, 0 });
    else
      steps_.push_back(s);
    sum_ += s.at_;
    return *this;
  }

  /** \brief Add a step to the gradient. **/
  gradient& operator <<(step step) noexcept {
    return operator+=(step);
  }

  /**
   * \brief Return the gradient with N steps as an `std::array`.
   * \tparam N Size of array.
   **/
  template <long N>
  std::array<color, N> make() const noexcept {
    std::array<color, N> grad {};
    std::fill(std::begin(grad), std::end(grad), start_);
    auto cit { std::begin(steps_) };
    auto nit { std::next(cit) };
    auto git { std::begin(grad) };
    while (nit != std::end(steps_)) {
      auto sz { std::max(std::lround(nit->at_ / sum_ * N), 1L) };
      auto g { interpolate(sz, cit->col_, nit->col_) };
      std::copy(std::begin(g), std::end(g), git);
      git += g.size();
      nit++; cit++;
    }
    return grad;
  }

  /**
   * \brief Return the gradient with N steps as an `std::vector`.
   * \param n Size of vector.
   **/
  std::vector<color> make(long n) const noexcept {
    std::vector<color> grad {};
    grad.reserve(n);
    std::fill(std::begin(grad), std::end(grad), start_);
    auto cit { std::begin(steps_) };
    auto nit { std::next(cit) };
    auto git { std::back_inserter(grad) };
    while (nit != std::end(steps_)) {
      auto sz { std::max(std::lround(nit->at_ / sum_ * n), 1L) };
      auto g { interpolate(sz, cit->col_, nit->col_) };
      std::copy(std::begin(g), std::end(g), git);
      nit++; cit++;
    }
    return grad;
  }

 private:
  ncs::color::color start_ { black };
  std::vector<step> steps_ {};
  decltype(std::declval<V>() + std::declval<V>()) sum_ {};
};

}  // namespace color
}  // namespace ncs

#endif  // NCSTREAM_NCS_COLOR_GRADIENT_H_
