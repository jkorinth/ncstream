#include "ncs/ncostream.h"
extern "C" {
#include <ncursesw/ncurses.h>
}
#include <algorithm>
#include <cassert>
#include <clocale>
#include <iterator>

namespace ncs {

ncostream::ncostream() noexcept {
  setlocale(LC_ALL, "");
  initscr();
  if (ncostream::has_colors()) {
    start_color();
  }
}

ncostream::~ncostream() noexcept {
  use_default_colors();
  endwin();
}

extent ncostream::screen_extent() noexcept {
  return { height { getmaxy(stdscr) + 1 }, width { getmaxx(stdscr) + 1 } };
}

bool ncostream::has_colors() noexcept {
  return ::has_colors();
}

bool ncostream::can_change_color() noexcept {
  return ::can_change_color();
}

int ncostream::max_number_of_colors() noexcept {
  return COLORS;
}

int ncostream::max_number_of_color_pairs() noexcept {
  return COLOR_PAIRS;
}

ncostream& ncostream::operator <<(const manipulator::clear c) noexcept {
  ::clear();
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::clrtoeol c) noexcept {
  ::clrtoeol();
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::refresh r) noexcept {
  ::refresh();
  return *this;
}

ncostream& ncostream::operator <<(const pos p) noexcept {
  ::move(p.row_, p.col_);
  return *this;
}

ncostream& ncostream::operator <<(const rel_pos rp) noexcept {
  const auto se { screen_extent() };
  ::move(rp.rrow_ * se.height_, rp.rcol_ * se.width_);
  return *this;
}

ncostream& ncostream::operator <<(const h h) noexcept {
  ::move(getcury(stdscr), getcurx(stdscr) + h);
  return *this;
}

ncostream& ncostream::operator <<(const v v) noexcept {
  ::move(getcury(stdscr) + v, getcurx(stdscr));
  return *this;
}

ncostream& ncostream::operator <<(const move m) noexcept {
  ::move(getcury(stdscr) + m.v_, getcurx(stdscr) + m.h_);
  return *this;
}

ncostream& ncostream::operator <<(const std::string& str) noexcept {
  addstr(str.c_str());
  return *this;
}

ncostream& ncostream::operator <<(const std::wstring& str) noexcept {
  addwstr(str.c_str());
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::set_cursor& sc) noexcept {
  curs_set(static_cast<int>(sc.vis_));
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::set_cursor_color& sc)
    noexcept {
  printf("\033]12;#%.2x%.2x%.2x\007",
      sc.col_.red_.to<unsigned char>(),
      sc.col_.green_.to<unsigned char>(),
      sc.col_.blue_.to<unsigned char>());
  fflush(stdout);
  return *this;
}

static inline unsigned int to_int(const attr a) noexcept {
  return static_cast<unsigned int>(a);
}

ncostream& ncostream::operator <<(const manipulator::set_attrs& sa) noexcept {
  unsigned int attrs { 0u };
  for (const auto a : sa.attrs_) {
    attrs |= to_int(a);
  }
  attrset(attrs);
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::set_color_pair& p) noexcept {
  int attrs, pair_id;
  attr_get(&attrs, &pair_id, nullptr);
  attr_set(attrs, p.pair_id_, nullptr);
  return *this;
}

ncostream& ncostream::operator <<(const manipulator::change_color& p) noexcept {
  mvchgat(p.pos_.row_, p.pos_.col_, 1, A_NORMAL, p.pair_id_, nullptr);
  return *this;
}

ncostream& ncostream::operator <<(const char c) noexcept {
  addnstr(&c, 1);
  return *this;
}

ncostream& ncostream::operator <<(const wchar_t c) noexcept {
  addnwstr(&c, 1);
  return *this;
}

}  // namespace ncs
