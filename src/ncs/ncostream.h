#ifndef NCSTREAM_NCS_NCOSTREAM_H_
#define NCSTREAM_NCS_NCOSTREAM_H_

#include <iterator>
#include <set>
#include <string>
#include "ncs/color/color.h"
#include "ncs/types.h"

/** \brief ncurses stream namespace **/
namespace ncs {

/** \brief ncurses stream manipulators **/
namespace manipulator {

/** \brief Clears the screen. **/
struct clear final {};

/** \brief Clears until end of line. **/
struct clrtoeol final {};

/** \brief Outputs/updates the visible screen. **/
struct refresh final {};

/** \brief Sets the cursor visibility. **/
struct set_cursor final {
  const cursor_vis vis_;                /**< \brief Visibility mode. **/
};

/** \brief Sets the cursor color. **/
struct set_cursor_color final {
  const color::color col_;              /**< \brief Color. **/
};

/** \brief Sets attributes. **/
struct set_attrs final {
  const std::set<attr> attrs_;          /**< \brief Attribute set. **/
};

/** \brief Activates the color pair (fg/bg). **/
struct set_color_pair final {
  const color::color_pair_id pair_id_;  /**< \brief Color pair id. **/
};

/** \brief Changes the color at pos to the color pair. **/
struct change_color final {
  const ncs::pos pos_;                       /**< \brief Position to change. */
  const ncs::color::color_pair_id pair_id_;  /**< \brief Color pair id. **/
};

/** \brief Logical or operator on attributes. **/
inline set_attrs operator |(const set_attrs a, const set_attrs b) {
  std::set<attr> attrs { a.attrs_ };
  attrs.insert(std::begin(b.attrs_), std::end(b.attrs_));
  return { attrs };
}

}  // namespace manipulator

/**
 * \addtogroup Manipulators Manipulator shorthands
 * @{
 **/

/** \brief Clear screen manipulator. **/
constexpr manipulator::clear clear = {};
/** \brief Clear until end-of-line manipulator. **/
constexpr manipulator::clrtoeol clrtoeol = {};
/** \brief Refresh screen manipulator. **/
constexpr manipulator::refresh refresh = {};
/** \brief Convenience shortcut: hide cursor. **/
constexpr manipulator::set_cursor hide_cursor = { cursor_vis::invisible };
/** \brief Convenience shortcut: show normal cursor. **/
constexpr manipulator::set_cursor show_cursor = { cursor_vis::normal };
/** \brief Convenience shortcut: show emphasized cursor. **/
constexpr manipulator::set_cursor emph_cursor = { cursor_vis::very_visible };

/** @} **/

/**
 * \addtogroup Attributes Font Attributes
 * @{
 **/

/** \brief Reset all font attributes. **/
const manipulator::set_attrs reset        = { {} };
/** \brief Use normal font face. **/
const manipulator::set_attrs normal       = { { attr::normal } };
/** \brief Use underlining. **/
const manipulator::set_attrs underline    = { { attr::underline } };
/** \brief Render inverted background/foreground. **/
const manipulator::set_attrs reverse      = { { attr::reverse } };
/** \brief Render blinking. **/
const manipulator::set_attrs blink        = { { attr::blink } };
/** \brief Render dimmed. **/
const manipulator::set_attrs dim          = { { attr::dim } };
/** \brief Use bold font face. **/
const manipulator::set_attrs bold         = { { attr::bold } };
/** \brief Render invisibly. **/
const manipulator::set_attrs invis        = { { attr::invis } };
/** \brief Use italic font face. **/
const manipulator::set_attrs italic       = { { attr::italic } };

/** @} **/

/**
 * \brief Top-left position (origin) of terminal screen.
 **/
constexpr pos origin = { row { 0 }, col { 0 } };

/**
 * \brief ncurses output stream
 *
 * `ncostream` activates `ncurses` on construction and provides stream operators
 * to output text, move the cursor, change colors, and other things necessary to
 * render `ncurses` based command line output.
 **/
class ncostream  {
 public:
  ncostream() noexcept;
  virtual ~ncostream() noexcept;

  /** \return current extent of the current terminal window. **/
  static extent screen_extent() noexcept;
  /** \return true, iff. terminal supports colors. **/
  static bool has_colors() noexcept;
  /** \return true, iff. terminal supports changing of colors. **/
  static bool can_change_color() noexcept;
  /** \return number of colors supported by terminal. **/
  static int max_number_of_colors() noexcept;
  /** \return number of color pairs (fg/bg) supported by terminal. **/
  static int max_number_of_color_pairs() noexcept;

  ncostream& operator <<(const manipulator::clear c) noexcept;
  ncostream& operator <<(const manipulator::clrtoeol c) noexcept;
  ncostream& operator <<(const manipulator::refresh r) noexcept;
  ncostream& operator <<(const pos p) noexcept;
  ncostream& operator <<(const rel_pos rp) noexcept;
  ncostream& operator <<(const h h) noexcept;
  ncostream& operator <<(const v v) noexcept;
  ncostream& operator <<(const move m) noexcept;
  ncostream& operator <<(const std::string& str) noexcept;
  ncostream& operator <<(const std::wstring& str) noexcept;
  ncostream& operator <<(const manipulator::set_cursor& sc) noexcept;
  ncostream& operator <<(const manipulator::set_cursor_color& sc) noexcept;
  ncostream& operator <<(const manipulator::set_attrs& sa) noexcept;
  ncostream& operator <<(const manipulator::set_color_pair& p) noexcept;
  ncostream& operator <<(const manipulator::change_color& p) noexcept;
  ncostream& operator <<(const char c) noexcept;
  ncostream& operator <<(const wchar_t c) noexcept;
};

}  // namespace ncs

#endif  // NCSTREAM_NCS_NCOSTREAM_H_
