#ifndef NCSTREAM_NCS_TYPES_H_
#define NCSTREAM_NCS_TYPES_H_

extern "C" {
#include "ncursesw/ncurses.h"
}
#include <cmath>

namespace ncs {

/**
 * \brief Generic base type of a coordinate value in coordinate system.
 * \tparam T Underlying type used to represent the value (default: `int64_t`).
 **/
template <typename T = int64_t>
struct coordinate {
  using type = T;
  /** \brief Construct coordinate from value v. **/
  explicit constexpr coordinate(const T v) : value_(v) {}
  /** \return Value of the coordinate. **/
  operator T() const noexcept { return value_; }

 private:
  T value_;
};

/** \brief A row in a terminal screen. **/
struct row : public coordinate<> { using coordinate::coordinate; };
/** \brief A column in a terminal screen. **/
struct col : public coordinate<> { using coordinate::coordinate; };

/** \brief A position in a terminal screen. **/
struct pos {
  row row_;     /**< \brief The vertical coordinate, i.e., row. **/
  col col_;     /**< \brief The horizontal coordinate, i.e., column. **/
};

/**
 * \brief A vertical distance expressed as a factor to be multiplied with the
 * current terminal screen height.
 *
 * _Example:_ `rel_row {0.5f}` represents the vertical center of the screen.
 **/
struct rel_row : public coordinate<float> { using coordinate::coordinate; };

/**
 * \brief A horizontal distance expressed as a factor to be multiplied with the
 * current terminal screen width.
 *
 * _Example:_ `rel_col {0.5f}` represents the horizontal center of the screen.
 **/
struct rel_col : public coordinate<float> { using coordinate::coordinate; };

/**
 * \brief Represents a position relative to current terminal screen size.
 **/
struct rel_pos {
  rel_row rrow_;  /**< \brief The relative row. **/
  rel_col rcol_;  /**< \brief The relative column. **/
};

/** \brief Height in screen rows. **/
struct height : public coordinate<> { using coordinate::coordinate; };
/** \brief Width in screen columns. **/
struct width : public coordinate<> { using coordinate::coordinate; };

/**
 * \brief Extent in screen coordinates (rows and columns).
 **/
struct extent {
  height height_;   /**< \brief Vertical extent in rows. **/
  width width_;     /**< \brief Horizontal extent in columns. **/
};

/**
 * \brief A vertical extent expressed as a factor to be multiplied with the
 * current terminal screen height.
 *
 * _Example:_ `rel_height {1.0f}` gives the number of rows in current screen.
 **/
struct rel_height : public coordinate<float> {};

/**
 * \brief A horizontal extent expressed as a factor to be multiplied with the
 * current terminal screen width.
 *
 * _Example:_ `rel_width {1.0f}` gives the number of columns in current screen.
 **/
struct rel_width  : public coordinate<float> {};

/**
 * \brief A two-dimensional screen extent expressed as relative factors to
 * current screen size.
 **/
struct rel_extent {
  rel_height rheight_;  /**< \brief Relative height. **/
  rel_width rwidth_;    /**< \brief Relative width. **/
};

/** \brief A vertical movement in rows. **/
struct v : public coordinate<> { using coordinate::coordinate; };
/** \brief A horizontal movement in cols. **/
struct h : public coordinate<> { using coordinate::coordinate; };

/** \brief A movement in screen coordinates (rows and columns). **/
struct move {
  v v_;     /**< \brief Vertical movement. **/
  h h_;     /**< \brief Horizontal movement. **/
};

/** \brief Cursor visibility modes. **/
enum class cursor_vis {
  invisible,      /**< Hide cursor. **/
  normal,         /**< Show cursor in the default way of the terminal. **/
  very_visible    /**< Emphasize cursor (terminal support required). **/
};

/** \brief Font attributes. **/
enum class attr : unsigned int {
  blink       = A_BLINK,        /**< Render blinking. **/
  bold        = A_BOLD,         /**< Bold font face rendering. **/
  dim         = A_DIM,          /**< Render dimmed. **/
  invis       = A_INVIS,        /**< Invisible. **/
  italic      = A_ITALIC,       /**< Italic font face rendering. **/
  normal      = A_NORMAL,       /**< Normal font face rendering. **/
  protect     = A_PROTECT,      /**< Protected rendering (see ncurses doc). **/
  reverse     = A_REVERSE,      /**< Render with switched fg/bg colors. **/
  underline   = A_UNDERLINE,    /**< Underlined font rendering. **/
};

/**
 * \addtogroup Position Position arithmetic
 * @{
 **/

/** \return p moved vertically by v. **/
inline pos operator +(const pos p, const v v) {
  return pos { row {p.row_ + v}, p.col_ };
}

/** \return p moved horizontally by h. **/
inline pos operator +(const pos p, const h h) {
  return pos { p.row_, col {p.col_ + h} };
}

/** \return p moved vertically by -v. **/
inline pos operator -(const pos p, const v v) {
  return pos { row {p.row_ - v}, p.col_ };
}

/** \return p moved horizontally by -h. **/
inline pos operator -(const pos p, const h h) {
  return pos { p.row_, col {p.col_ - h} };
}

/** @} **/

}  // namespace ncs

#endif  // NCSTREAM_NCS_TYPES_H_
